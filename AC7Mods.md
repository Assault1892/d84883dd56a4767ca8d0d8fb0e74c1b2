# 強化系

- [AC7 Enhanced Gunplay Mod - Gauntlet Edition (SP) A4.1](https://www.nexusmods.com/acecombat7skiesunknown/mods/1480)

  - 高難易度化 Mod

  ```
  導入済みPaks:
  1. ~~~EOM_EnhancedGunplay_P.pak (機銃強化)
  2. ~~~EOM_EnhancedGunplay_Raycast_P.pak (機銃レイキャスト改善)
  3. ~~~EOM_EnhancedGunplay_PlaneParts_P.pak (機体パーツ調整 + 全パーツ装備可能)
  4. ~~~EOM_EnhancedGunplay_AI_P.pak (AI改善)
  5. ~~~EOM_EnhancedGunplay_AI_Weapon_Ace_P.pak (敵性能超強化)
  6. ~~~EOM_EnhancedGunplay_AllAmmo2X_P.pak (残弾2倍)
  ```

- [Semi-real Flight Model Pack v1.3](https://www.nexusmods.com/acecombat7skiesunknown/mods/2075)
  - 機体の飛行パラメーター大幅変更 実機基準になるらしい

# UI 系

- [Ace Combat 7 - Know Your Enemy](https://www.nexusmods.com/acecombat7skiesunknown/mods/1384)

  - HUD 賑やかし (効果あるのか分からん 日本語 UI だと一部無効になるかも)

- [TLS PLSL EML Reticle mod (Hex Reticle)](https://www.nexusmods.com/acecombat7skiesunknown/mods/328)

  - TLS, PLSL, EML に見やすいレティクルを追加する

# グラフィック系

- [Better Flares Vision Mod](https://www.nexusmods.com/acecombat7skiesunknown/mods/1473)

  - フレアの見た目を綺麗にする

- [Colored Afterburners (Scarlet)](https://www.nexusmods.com/acecombat7skiesunknown/mods/1282)
  - アフターバーナーを綺麗にする
